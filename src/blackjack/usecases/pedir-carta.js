/**
 * Esta funcion pide una carta del deck
 * @param {Array<String>} deck Ejemplo ["JD", "QD", "8C", "9S", ...]
 * @returns {String} retorna una carta del deck
 */
export const pedirCarta = (deck) => {

    if ( deck.length === 0 || deck.length === 0 ) {
        throw 'No hay cartas en el deck';
    }
    const carta = deck.pop();
    return carta;
}