/**
 * Esta función calcula el valor de la carta
 * @param {String} carta Ejemplo 'AH'
 * @returns retorna el valor numérico de la carta
 */
export const valorCarta = ( carta ) => {

    if (!carta) throw new Error('Debe proporcionar una carta');

    if (!carta) throw new Error('Debe proporcionar una carta');

    const valor = carta.substring(0, carta.length - 1);
    return ( isNaN( valor ) ) ? 
            ( valor === 'A' ) ? 11 : 10
            : valor * 1;
}